---
layout: default
title: Reviews
---

# Customer Reviews

Here are some reviews from our satisfied customers:
<div class="customer-icon-wrapper">
  <div class="customer-icon">
    <!-- Add the customer's photo here -->
    <img src="./assets/images/customer-photo.png" alt="Customer Photo">
  </div>
  <div class="customer-review">
    <div class="customer-name"> </div>
    <div class="customer-comment">"Semla tyckte att det var väldigt spännande och roligt, hon berättade till sin pappa också." - Fetimah</div>
  </div>
</div>

<div class="review">
  <div class="stars">
    ★★★★★
  </div>
  <div class="content">
    <p>"Semla tyckte att det var väldigt spännande och roligt, hon berättade till sin pappa också." - Fetimah</p>
  </div>
</div>

<div class="review">
  <div class="stars">
    ★★★★★
  </div>
  <div class="content">
    <p>"Nulla eget felis et magna placerat eleifend." - Jane Smith</p>
  </div>
</div>

<div class="review">
  <div class="stars">
    ★★★★★
  </div>
  <div class="content">
    <p>"Aenean volutpat magna vitae ante consectetur, sed bibendum magna malesuada." - David Johnson</p>
  </div>
</div>

