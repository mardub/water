---
layout: default
title: FAQ
---

# Frequently Asked Questions


**Q:** I cannot make it on sunday evening, can you do it on another day?

**A:** Contact me. I may be able to but I have to check with the swimming pool if the deep pool is available. I plan to make more times available in the future.

**Q:** Can you pick up my kid from home?

**A:** No, but I can take your kid from the busstation Skolgatan to Fyrishov before and after the session.


**Q:** What age do you accept

**A:** The courses have been tested on 8 years old kids onward. Younger may need to be supervised by their parents.

**Q:** Why this price?
**A:** For the moment I am not making benefit from this. It is not even the price of a baby sitter. The price is a compensation to cover the cost of my material use, my swim abonment, and the bus to go to the pool. 

