---
layout: default
title: Teacher

---

Experience: 10 years of freediving and a certification CMAS. I was bronze and gold medalist of sweden. I created a new hobby: the dance underwater. Throughout the years I perfected the  technics to look beautiful, feel relaxed and stay safe underwater. My approach to breath holding is challenging the competitive view of apnea to focus on the artistic and relaxing experience. Today I want to share my art to as many as possible.

Style: A mix of on pop, electro, it's all about groove!
