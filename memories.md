---
layout: default
title: Memories
---

# Memories

Here are some cherished memories:

<div class="carousel">
  <div class="movie">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/n7b8Rbwq5jE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <p>This was a parodic video made for the linguistic department of Uppsala University by an enthousiastic colleague attending a session</p>
  </div>
  <div class="movie">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/n7b8Rbwq5jE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <p>Semla from her 8 years old is not scared to fetch the weight!</p>
  </div>
  <div class="movie">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/n7b8Rbwq5jE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    <p>Memory 3</p>
  </div>
</div>

<script>
// Slick Carousel initialization
$(document).ready(function() {
  $('.carousel').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    prevArrow: false,
    nextArrow: false
  });
});
</script>

